// HashSet internally calls the put() method of HashMap and then hashing function
// Duplicate key not allowed
// Hashcode of string formual :
// e.g RAM (noofchar n = 3)
// hashcode = ascii[R]*31^(n-1) + ascii[A]*31^(n-2) + ascii[M]*31^(n-3)
// for the integer the numbers stored in bucket is printed opposite to string
// for string = up to bottom
// for int = from bottom to up
//
// The only difference between HashMap and LinkedHashMap is insertion order is preserved in
// LHM
// Iterator can be used on HashMap by converting it into set using entrySet() function
// Set<Map$Entry<k,v>> entrySet()
// p abstract V get(Object)
// p abstract V remove(Object)
// p abstract set<k> keySet()
// Collection<V> get()

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		HashMap hm = new HashMap();

		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Rahul":"BMC");
		//hm.put("Kanha":"L&T");

		System.out.println(hm);
	}
}
