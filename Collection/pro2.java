import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		al.add(10);
		al.add(10.55);
		al.add(10.65f);
		al.add("Dhiraj");
		al.add('D');

		// not a proper way to access data
		for(var x:al) {		// error for var if version <=1.8
		
			System.out.println(x.getClass().getName());
		}
	}
}
