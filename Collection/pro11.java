// Treeset

import java.util.*;
/*
class Demo {

	public static void main(String[] args) {
	
		TreeSet ts = new TreeSet();

		ts.add("Dhiraj");
		ts.add("Vinay");
		ts.add("Jd");
		ts.add("Sachin");

		System.out.println(ts);
	}
}*/

class MyClass implements Comparable {

	String str;

	MyClass(String str) {
	
		this.str = str;
	}
	public int compareTo(Object obj) {
		
		System.out.println("1" + str);
		System.out.println("2" + ((MyClass)obj).str);
		return str.compareTo(((MyClass)obj).str);
	}
	public String toString() {
	
		return str;
	}
}

class Demo {

	public static void main(String[] args) {
	
		TreeSet ts = new TreeSet();
		
		ts.add(new MyClass("Dhiraj"));
		ts.add(new MyClass("Vinay"));
		ts.add(new MyClass("Jd"));
		ts.add(new MyClass("Sachin"));

		System.out.println(ts);
	}
}
