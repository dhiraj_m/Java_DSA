// finalize method is used to notify that object is cleaned by Garbage collection
// Equivalent to destructor in cpp
// The finalize() is called at the end of the program

import java.util.*;

class GCDemo {
	
	String str;

	GCDemo(String str) {
		
		this.str = str;
	}
	public String toString() {
		
		return str;
	}
	public void finalize() {
		
		System.out.println("Nofity!!!");
	}
	
}


class Demo {

	public static void main(String[] args) {
	
		GCDemo obj1 = new GCDemo("Vinay");
		GCDemo obj2 = new GCDemo("Lomate");

		System.out.println(obj1);
		System.out.println(obj2);

		obj1 = null;
		obj2 = null;

		System.gc();
		System.out.println("In main");
	}
}
