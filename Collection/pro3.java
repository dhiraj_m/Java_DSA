// Whenever object is added in arraylist toString method is called and in case of user defined
// class objects we have to override toString() method otherwise address of objects gets print
//

import java.util.*;
class CricPlayer {

	int jerNo;
	String pName;

	CricPlayer(int jerNo, String pName) {
	
		this.jerNo = jerNo;
		this.pName = pName;
	}
	public String toString() {
	
		return jerNo+" : "+pName;
	}
}

class Demo {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		al.add(new CricPlayer(18,"King"));
		al.add(new CricPlayer(7,"Mahi Bhai"));
		al.add(new CricPlayer(33,"Kali"));

		System.out.println(al);
	}
}
