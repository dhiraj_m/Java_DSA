class Demo {

	public static Parent fun() {
		
		return new Child();
	}
}
class Parent extends Demo {

	
}
class Child extends Parent {

	void gun() {
	
	}
}

class MyDemo {

	public static void main(String[] args) {
	
		Parent ch = Demo.fun();
		ch.gun();	
	}
}
