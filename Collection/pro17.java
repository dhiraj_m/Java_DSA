// Iterator on map


import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		HashMap hm = new HashMap();
		hm.put(10,"D");
		hm.put(20,"S");
		hm.put(30,"M");

		Set<Map.Entry>s = hm.entrySet();

		Iterator<Map.Entry> itr = s.iterator();
		while(itr.hasNext()) {
		
			//System.out.println(itr.next());
			Map.Entry abc = itr.next();
			System.out.println(abc.getKey()+" : "+abc.getValue());
		}
	}
}
