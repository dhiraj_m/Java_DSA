// HashTable:- same as HashMap but it is synchronized
// It is legacy member and hence support Enumeration
// HashTable extends Dictionary
// methods:
// 1.E keys();
// 2.E elements()
// 3.get(obj)
// 4.remove(obj)


import java.util.*;

class Demo {

	public static void main(String[] a) {
	
		Dictionary d = new Hashtable();

		d.put(1,"D");
		d.put(2,"S");
		d.put(3,"M");

		Enumeration itr1 = d.keys();
		while(itr1.hasMoreElements()) {
		
			System.out.println(itr1.nextElement());
		}

		Enumeration itr2 = d.elements();
		while(itr2.hasMoreElements()) {
		
			System.out.println(itr2.nextElement());
		}
	}
}
