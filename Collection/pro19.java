// Properties

import java.util.*;
import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		Properties obj = new Properties();
		
		FileInputStream fobj = new FileInputStream("friends.properties");
		obj.load(fobj);

		String name = obj.getProperty("Ashish");
		System.out.println(name);

		obj.setProperty("Sir","Biencaps");

		FileOutputStream outobj = new FileOutputStream("friends.properties");
		obj.store(outobj,"update by Dhiraj");
	}
}
