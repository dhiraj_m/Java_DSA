// WeakHashMap
// HashMap dominates the garbage collector
// That is object stored in the hashmap does not gets free by GC
// Hence to free object i.e to allow GC to free obj weakHashMap is used


import java.util.*;

class Demo {

	public static void main(String[]a) {
	
		Integer obj1 = new Integer(10);
		Integer obj2 = new Integer(20);
		Integer obj3 = new Integer(30);
		Integer obj4 = new Integer(40);
		Integer obj5 = new Integer(50);
		Integer obj6 = new Integer(60);

		HashMap hm = new HashMap();

		hm.put(obj1,"D");
		hm.put(obj2,"S");
		hm.put(obj3,"M");
		
		obj1 = null;
		obj2 = null;
		
		System.gc();

		System.out.println(hm);
		
		WeakHashMap whm = new WeakHashMap();

		whm.put(obj4,"D");
		whm.put(obj5,"S");
		whm.put(obj6,"M");
		
		obj4 = null;
		obj5 = null;
		
		System.gc();
		
		System.out.println(whm);
	}
}
