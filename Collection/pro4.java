// LinkedList
// In java LinkedList can be accessed by index as List supports indexing
// But LinkedList should support indexing as there are nodes which contains address of next

// 1.public E getFirst()
// 2.public E getLast()
// 3.public E removeFirst()
// 4.public E removeLast()
// 5.public E addFirst()
// 6.public E removeLast()
// 7.LinkedList$Node<E> node()

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.add(20);
		ll.add("Dhiraj");

		System.out.println(ll);
	}
}
