// HashSet
// Data should be unique
// Insertion order is not preserved
// It internally uses hashing function to store the data
// The data is divided by the number of buckets and in stored according the remainder 
// By default size of bucket is 11
// suppose data 12,33,45,19 then remainders will be 1,0,1,8 then 12,33,45,19 will be 
// stored at indices 1,0,1,8 hence o/p will be 19,45,12,33
// And for the string firstly the hashcode of string is calculated using hashcode formula
// and above procedure is followed

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		HashSet hs = new HashSet();
		hs.add("Dhiraj");
		hs.add("Vinay");
		hs.add("Jd");
		hs.add("Dhiraj");
		hs.add("Sachin");
		hs.add("Jd");

		System.out.println(hs);
	}
}
