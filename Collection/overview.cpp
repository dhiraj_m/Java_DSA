// Collection Interfaces :-
// 1. Collection :- stores objects(extends Iterable)
// 2. List :- allow duplicate data
// 3. Set :- doesn't allow duplicate data
// 4. SortedSet :- unique sorted data
// 5. Queue :- First in first out
// 6. Map :- stores values in the form of Key:Value pair(most used) JSON
// 7. SortedMap
// 8. NavigableMap
// 9. NavigableSet

Iterable(I)
Three Iterators
1.Iterator
2.ListIterator
3.enummeration
4.splitIterator

Collection hierarchy
1. List(I)
   1.ArrayList(c) :- 1.2 extends abstract class AbstractList 
   			abstract class AbstractList extends AbstractionCollection implements List
			abstract class AbstractCollection implements Collection,Iterable
   2.LinkedList(c) :- 1.2
   3.Stack(c) :- 1.0	extends Vector
   4.Vector(c) :- 1.0	extends AbstractList

2. Set(I)
   1.HastSet(c)
	1.LinkedHashSet(c)
   2.SortedSet(I)
	1.NavigableSet(I)
	2.TreeSet(c)

3. Queue(I)
   1.PriorityQueue(c)
   2.Dequeue(I)
   3.BlockingQueue(I)
	1.ArrayBlockingQueue(c)
	2.LinkedBlockingQueue(c)
	3.PriorityBlockingQueue(c)




Map(I)
1.HashMap(c)
	LinkedHashMap

2.WeakHashMap(c)
3.IdentityHashMap(c)
4.EnumMap(c)
5.Hashable
	Properties(c)
6.SortedMap
	
