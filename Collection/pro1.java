// List
// Allows duplicate data
// Insertion order preserved
//
// 1.ArrayList :- Can store different type of data

import java.util.*;

class ALDemo extends ArrayList {

	public static void main(String[] args) {
	
		//ArrayList<int> al = new ArrayList<int>();	// to avoid note specify which
								// type of data to be stored
		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20.5f);
		al.add("Dhiraj");
		al.add(20.5f);
		al.add(10);
		
		System.out.println(al);
		System.out.println(al.size());

		System.out.println(al.contains("Dhiraj"));
		System.out.println(al.contains(50));

		System.out.println(al.indexOf(20.5f));
		System.out.println(al.lastIndexOf(20.5f));

		System.out.println(al.get(2));
		System.out.println(al.set(3,"Vinay"));
		
		System.out.println(al);
		al.add(3,"JD");
		System.out.println(al);

		ArrayList al2 = new ArrayList();
		al2.addAll(al);
		System.out.println(al2);
		
		al2.addAll(3,al);
		System.out.println(al2);

	//	al2.removeRange(3,8); protected(create Demo d)
		System.out.println(al2);
	}
}
