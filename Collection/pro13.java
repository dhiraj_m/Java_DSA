// Duplicate key allowed

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		IdentityHashMap ihm = new IdentityHashMap();

		ihm.put(new Integer(10),"D");
		ihm.put(new Integer(10),"S");
		ihm.put(new Integer(10),"M");
		ihm.put(10,"M");

		System.out.println(ihm);
	}
}
