// Stack
// Insertion order preserved

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Stack s = new Stack();

		s.push(10);
		s.push("Dhiraj");
		s.push(10.44f);
		s.push("Tu pop");

		s.pop();

		System.out.println(s);
		System.out.println(s.peek());	//return top of stack
						
		Enumeration itr = s.elements();

		while(itr.hasMoreElements()) {
		
			System.out.println(itr.nextElement());
		}
	}
}
