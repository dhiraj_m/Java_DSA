// PriorityQueue 
// Stores data according some priority
// It uses heap data structure to store the values in Queue
// Heap internally implemented binary tree

import java.util.*;
class Demo {

	public static void main(String[] args) {
	
		PriorityQueue pq = new PriorityQueue();

		pq.offer(20);
		pq.offer(10);
		pq.offer(50);
		pq.offer(30);
		pq.offer(40);

		System.out.println(pq);
	}
}

