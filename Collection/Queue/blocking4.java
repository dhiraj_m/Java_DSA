// PriorityBlockingQueue

import java.util.concurrent.*;

class Demo {

	public static void main(String[] args)throws InterruptedException {
	
		BlockingQueue q = new PriorityBlockingQueue(3);

		q.offer(10);
		q.offer(20);
		q.offer(30);
		q.offer(40);

		q.put(50);

		System.out.println(q);
	}
}
