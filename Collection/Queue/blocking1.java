// Blocking Queue(Bounded Queue)
// For synchronization blocking queue is used
// Queue is locked when thread is working on queue
// BlockingQueue(I)
// 	1.ArrayBlockingQueue(c)
// 	2.LinkedBlockingQueue(c)
// 	3.PriorityBlockingQueue(c)
//

import java.util.concurrent.*;

class Demo {

	public static void main(String[] args)throws InterruptedException {
	
		BlockingQueue bq = new ArrayBlockingQueue(3);

		bq.offer(10);
		bq.offer(20);
		bq.offer(30);
		//bq.offer(35);		// does not get added as queue is full

		//bq.put(40);	// waits for other thread to remove previous data
				// so that 40 can be added(put in blockingQ throws Exception)
				// (blocking situation) main thread is blocked here
				// Hence while working with blocking queue create multiple
				// threads 1 thread is to add data and other is to remove data
		System.out.println(bq);
	}
}
