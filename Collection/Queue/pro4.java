// Dequeue

import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Deque d = new ArrayDeque();

		d.offer(10);
		d.offer(20);
		d.offer(30);
		d.offer(40);

		System.out.println(d);

		d.offerFirst(5);
		d.offerLast(50);

		System.out.println(d);

		d.pollFirst();
		d.pollLast();

		System.out.println(d);

		System.out.println(d.peekFirst());
		System.out.println(d.peekLast());

		Iterator itr = d.descendingIterator();

		while(itr.hasNext()) {
		
			System.out.println(itr.next());
		}
	}
}
