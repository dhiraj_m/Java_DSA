import java.util.*;

class Project implements Comparable {

	String proName;
	int teamSize;
	int duration;

	Project(String proName,int teamSize,int duration) {
	
		this.proName = proName;
		this.teamSize = teamSize;
		this.duration = duration;
	}
	public String toString() {
	
		return "{"+proName+":"+teamSize+":"+duration+"}";
	}
	public int compareTo(Object obj) {
	
		return this.duration - ((Project)obj).duration;
	}
}
class SortByName implements Comparator {

	public int compare(Object obj1,Object obj2) {
	
		return ((Project)obj1).proName.compareTo(((Project)obj2).proName);
	}
}

class Demo {

	public static void main(String[] args) {
	
		//PriorityQueue pq = new PriorityQueue();
		PriorityQueue pq = new PriorityQueue(new SortByName());

		pq.offer(new Project("XYZ",25,18));
		pq.offer(new Project("PQR",10,8));
		pq.offer(new Project("ABC",5,3));

		System.out.println(pq);
	}
}
