// Queue
// poll() and remove() both removes the element the only differece is poll() does not throw 
// exception when Queue is empty while remove() throws NoElementException
// peek() and element() both returns the first(peek) value present, the difference between 
// them is peek() returns null if queue id empty and element() throws NoSuchElementException

import java.util.*;

class Demo {

	public static void main(String[] args) { 
	
		Queue q = new LinkedList();

		q.offer(10);
		q.offer(20);
		q.offer(30);

		System.out.println(q);

		q.poll();
		q.remove();

		System.out.println(q);

		System.out.println(q.peek());
		System.out.println(q.element());
	}
}
