import java.util.*;
import java.util.concurrent.*;

class Demo {

	public static void main(String[] args)throws InterruptedException {
	
		BlockingQueue b = new ArrayBlockingQueue(3);

		b.put(10);
		b.put(20);
		b.put(30);

		b.offer(40,5,TimeUnit.SECONDS);	// waits for 5 seconds for previous data to 
						// get free and then executes next lines
						// (blocked for specific time)if other thread
						// removes previous data in this time then
						// this data gets added
		System.out.println(b);
		b.take();			// throws InterruptedException
		System.out.println(b);

		ArrayList al = new ArrayList();
		System.out.println("ArrayList : "+al);
		b.drainTo(al);
		System.out.println("ArrayList : "+al);
	}
}
