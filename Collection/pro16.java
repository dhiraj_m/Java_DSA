// TreeMap
// Data stored in sorted manner


import java.util.*;

class PlatForm implements Comparable{

	String str;
	int year;

	PlatForm(String str, int year) {
	
		this.str = str;
		this.year = year;
	}
	public String toString() {
	
		return str+" : "+year;
	}
	public int compareTo(Object obj) {
	
		return this.year - ((PlatForm)obj).year;
	}
}
class Demo {

	public static void main(String[] a) {
	
		TreeMap tm = new TreeMap();

		tm.put(new PlatForm("Youtube",2005),"Google");
		tm.put(new PlatForm("Insta",2013),"Meta");
		tm.put(new PlatForm("FB",2004),"Meta");
		tm.put(new PlatForm("ChatGPT",2023),"OpenAI");

		System.out.println(tm);
	}
}
