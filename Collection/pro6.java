// Vector 
// Default capacity 10
// If capacity gets fulled then it is increased by 2*curren capacity
// This is legacy class that is present from verson 1.0
// Methods in this class does not throw any exception as there were no exceptions in 1.0

// try Vector methods
import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		Vector v = new Vector();

		v.addElement(10);
		v.addElement(10.55f);
		v.addElement("Dhiraj");

		System.out.println(v);

		// Enumeration iterator		
		Enumeration itr = v.elements();

		while(itr.hasMoreElements()) {
		
			System.out.println(itr.nextElement());
		}
	}
}
