// Cursors
// 1.Iterator(Universal and Unidirectional)
// 2.ListIterator(Only for List and Bidirectional)
// 3.Enumeration(for legacy classes Stack and Vector)
// 4.splitIterator
//
// In iterator we can get the specificc data needed while for loop prints all the data 


import java.util.*;

class Demo {

	public static void main(String[] args) {
	
		ArrayList al = new ArrayList();

		al.add("Dhiraj");
		al.add("Vinay");
		al.add("JD");

		Iterator itr = al.iterator();

		while(itr.hasNext()) {
/*		
			if("Vinay".equals(itr.next()))
				itr.remove();
*/
			System.out.println(itr.next());
		}
		
		// ListIterator	
		ListIterator litr = al.listIterator();
		while(litr.hasNext()) {
		
			System.out.println(litr.next());
		}
	}
}
