import java.io.*;

class BufferDemo {

	public static void main(String[] args) throws IOException{
	
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		int x = Integer.parseInt(br.readLine());
		char ch = (char)br.read();

		System.out.println(x);
		System.out.println(ch);
	}
}
