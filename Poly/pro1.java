class Parent{}

class Child extends Parent{}

class Demo {

	void p1(Parent p) {System.out.println("In parent");}
	void p1(Child c) {}
	public static void main(String[] args) {
	
		Demo d = new Demo();
		d.p1(new Child());
	}
}
