class StringDemo {

	static String str1 = "Dhiraj More";
	public static void main(String[] args) {
	
		String str2 = "Dhiraj More";
		String str3 = "Dhiraj More";
		String str4 = new String("Dhiraj More");
		String str5 = new String("Dhiraj More");

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
		System.out.println(System.identityHashCode(str5));
		
	}
}
