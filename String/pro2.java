class Demo {


	public static void fun(int[] arr) {
	
		arr[1] = 7000;
		arr[2] = 8000;
		
		System.out.println("In Fun");
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));
	}

	public static void main(String[] args) {
	
		int[] arr = {1000,2000,3000,4000};

		System.out.println(System.identityHashCode(arr));
		System.out.println(arr[0]);
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));
		
		//fun(arr);
		
		System.out.println("After Fun");
		//int x = 7000;
		//int y = 8000;
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[1]));
		System.out.println(System.identityHashCode(arr[2]));
		System.out.println(System.identityHashCode(arr[3]));
	}
}
