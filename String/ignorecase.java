class Demo {

	static boolean myStrEqualsIgnore(String str1, String str2) {
	
		char[] arr1 = str1.toCharArray();
		char[] arr2 = str2.toCharArray();

		int flag = 0;

		if(arr1.length == arr2.length) {
			for(int i=0; i<arr1.length; i++) {
			
				if(arr1[i] == arr2[i] || arr1[i]+32 == arr2[i] || arr2[i]+32 == arr1[i]) {
				
					continue;
				} else {
				
					flag = 1;
					break;
				}
			}
			if(flag == 0) {
			
				return true;
			} else {
			
				return false;
			}
		} else {
		
			return false;
		}
	}
}
