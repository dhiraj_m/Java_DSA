import java.util.*;
import java.io.*;

class Demo {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();
		StringTokenizer st = new StringTokenizer(str," ");

		while(st.hasMoreElements()) {
		
			System.out.println(st.nextToken());
		}
	}
}
