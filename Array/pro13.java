
class Demo {

	public static void main(String[] args) {
	
		int arr[][] = new int[][]{{100,200},{300,400}};
		int arr2[] = new int[]{100,200};

		System.out.println(System.identityHashCode(arr));
		System.out.println(System.identityHashCode(arr[0]));
		System.out.println(System.identityHashCode(arr[0][0]));
		
		System.out.println(System.identityHashCode(arr2));
		System.out.println(System.identityHashCode(arr2[0]));
	}
}
