import java.io.*;

class LargestEle {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements in the array");
		for(int i=0; i<arr.length; i++) {
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int max = arr[0];
		for(int i=0; i<arr.length; i++) {	//i can be initialized to 1
		
			if(arr[i] > max) {
			
				max = arr[i];
			}
		}
		System.out.println("Maximun element in the array = "+max);
	}
}
