import java.io.*;

class EvenOddSum {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements in the array");
		for(int i=0; i<arr.length; i++) {
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int esum = 0, osum = 0;
		for(int i=0; i<arr.length; i++) {
		
			if(arr[i]%2 == 0) {
			
				esum = esum + arr[i];
			} else {
			
				osum = osum + arr[i];
			}
		}
		System.out.println("Sum of even elements = "+esum);
		System.out.println("Sum of odd elements = "+osum);
	}
}
