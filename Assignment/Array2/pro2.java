import java.io.*;

class EvenOddCount {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements in the array");
		for(int i=0; i<arr.length; i++) {
		
			arr[i] = Integer.parseInt(br.readLine());
		}
		int ecount = 0, ocount = 0;
		for(int i=0; i<arr.length; i++) {
		
			if(arr[i]%2 == 0) {
			
				ecount++;
			} else {
			
				ocount++;
			}
		}
		System.out.println("Number of even elements = "+ecount);
		System.out.println("Number of odd elements = "+ocount);
	}
}
