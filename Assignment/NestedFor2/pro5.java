
class Pat5 {

	public static void main(String[] args) {
	
		int rows = 4;
		int num = 26;
		char ch = 'Z';

		for(int i=1; i<=rows; i++) {
		
			for(int j=1; j<=rows; j++) {
			
				if(j%2 == 1) {
				
					System.out.print(num-- + "  ");
				}else {
				 
					System.out.print(ch-- + "  ");
				}
			}
			System.out.println();
		}
	}
}
