class Pat9 {

	public static void main(String[] args) {
	
		int rows = 3;
		int num = 1;

		for(int i=1; i<=rows; i++) {
		
			int ch = 64 + rows;
			for(int j=1; j<=rows; j++) {
			
				System.out.print(num*num+""+(char)ch-- +(rows+1-j)+"  ");
				num++;
			}
			System.out.println();
		}
	}
}
