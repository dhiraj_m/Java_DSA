class Pat10 {

	public static void main(String[] args) {
	
		int rows = 6;

		for(int i=1; i<=rows; i++) {
		
			int ch = 64 + rows;
			int num = rows;
			for(int j=1; j<=rows; j++) {
			
				if(j%2 == 0)
					System.out.print(num+"  ");
				else
					System.out.print((char)ch+"  ");

				ch--;
				num--;
			}
			System.out.println();
		}
	}
}
