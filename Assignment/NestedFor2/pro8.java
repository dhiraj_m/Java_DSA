class Pat8 {

	public static void main(String[] args) {
	
		int rows = 4;
		char ch1 = 'A';
		char ch2 = 'a';

		for(int i=1; i<=rows; i++) {
		
			for(int j=1; j<=rows; j++) {
			
				if(j%2 == 1)
					System.out.print(ch1+"  ");
				else
					System.out.print(ch2+"  ");

				ch1++;
				ch2++;
			}
			System.out.println();
		}
	}
}
