class Demo {

	int x = 10;
	Demo() {

		int x = 20;		
		System.out.println(x);
	}
	static {
	
		System.out.println("Static block");
		Demo d = new Demo();
	}
	public static void main(String[] args) {
	
		System.out.println("Main");
	}
}
