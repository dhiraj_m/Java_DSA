class StaticDemo {

	static int x = 10;
	static int y;
	static {

		y = 30;	
		x = 30;
	}

	public static void main(String[] args) {
	
		System.out.println(x);
	}
	static {
	
		x = 20;
	}
}
