// finally:- error occurrs or not the code inside finally block will be executed compulsorily
// If we open server/port/socket connections and exception occurrs then code will be terminated abnormally but the connections are not closed
// Hence write the code to close the connection inside the finally block so that even if the exception is occurred it will visit the finally block and connections are closed


class Demo {

	public static void main(String[] args) {
	
		try {
		
			System.out.println(10/0);
		} catch(ArithmeticException ae) {
		
			System.out.println("Here");
		} finally {
		
			System.out.println("Conn closed");
		}
	}
}
