// Exception :- Natural flow of program execution stops due to uncertain events i.e is called exception
// The line at which exception occurs the whole code is terminated from that
// Single code have an multiple exceptions
// e.g Going to somewhere :- In this case exxception can be :- type punctured, accident etc


// ArithmeticException
// At the time of input/output stream can be closed due to any failure(stream can be closed using close() method), hence readLine() throws IOException
// To ensure normal flow we've to throw it or handle it

import java.io.*;

class Demo {

	public static void main(String[] args) {//throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		br.readLine();
	}
}

/*
	IOException in runtime

class Demo2 {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();
		br.close();
		String str2 = br.readLine();
	}
}
*/
