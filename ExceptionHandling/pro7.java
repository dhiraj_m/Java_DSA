// Handling multiple exceptions in single catch block

class Demo {

	public static void main(String[] args) {
	
		try {
		
			System.out.println(10/0);
		} catch(ArithmeticException | NullPointerException obj) {
		
		
		} /*catch(ArithmeticException | Exception obj) {

			error never use Exception in catch using OR 
		}*/
	}
}
