
import java.io.*;
class Demo {

	void fun() throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		br.close();
		int a = Integer.parseInt(br.readLine());

	}
	public static void main(String[] args) throws IOException{
	
		Demo obj = new Demo();
		obj.fun();
	}
}
