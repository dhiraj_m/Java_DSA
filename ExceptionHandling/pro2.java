// Exception hierarchy:-
// Object <- Throwable <- Exception/Error 
// Exception
// A.Checked Exceptions(Compile time)
// 1.IOException(io package)
// 2.FileNotFoundException(io package) extends IOException
// 3.InterruptedException(lang)




// B.UncheckedExeption(RuntimeException(it is a class))
// Exception <- RuntimeException
// 1.ArithmeticException
// 2.NullPointerException
// 3.IndexOutOfBoundsException
// 	i.ArrayIndexOutOfBoundsException
// 	ii.StringIndexOutOfBoundsException



// Runtime exceptions cannot be handled by throwing it to Default Exception Handler(i.e using throw)
// Hence try catch is used

// ArrayIndexOutOfBoundsException extends IndexOutOfBoundsException
// IndexOutOfBoundsException extends RuntimeException
// RuntimeException extends Exception

class Demo {

	public static void main(String[] args) {
	
		int arr[] = new int[]{1,2,3,4,5};

		for(int i=0;i<=arr.length; i++) {
		
			System.out.println(arr[i]);
		}
		/*
		try {
		
			for(int i=0;i<=arr.length; i++) {
		
				System.out.println(arr[i]);
			}
		} catch(ArrayIndexOutOfBoundsException) {
		
		
		}*/
	}
}

