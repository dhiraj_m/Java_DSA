// *****
// We have to extend our class with any class of Exception class hierarchy
// If we extend our class with Runtime class then exception will be thrown at runtime
// If we extend our class with IOException(checked exception) then exception will occur at
//compile time and we have to handle it otherwise compiler will give error unreportedexception// Throwable class has all the methods 
// Hence to print our msg we have to deliver that msg to the Throwable class constructor
// Throwable class prints our msg as well as predefined exception description

import java.util.*;

class DataOverFlowException extends RuntimeException {

	DataOverFlowException(String msg) {
	
		super(msg);
	}
}
class DataUnderFlowException extends RuntimeException {

	DataUnderFlowException(String msg) {

		super(msg);
	}
}

class ArrayDemo {

	public static void main(String[] args) {
	
		int arr[] = new int[5];
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter data(0<data<100)");
		for(int i=0; i<arr.length; i++) {
		
			int data = sc.nextInt();
			if(data < 0)
				throw new DataOverFlowException("less than 0 value");
			if(data > 100)
				throw new DataUnderFlowException("greater than 100 value");

			arr[i] = data;
		}  
	}
}
