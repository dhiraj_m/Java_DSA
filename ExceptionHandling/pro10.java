// Exception table
// from			to	  		target
// (line no.		(line where   		(Proper execution 
// from where 		try block ends)          line no. then JVM use goto and jump on 
// try block 					 this catch where the exception is handled)
// started)	       		
//
// If we used try catch then only exception table gets created
