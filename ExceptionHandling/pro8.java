// User Defined Exceptions 
// throw :- used to throw the exception
// throw is generally used to throw the user defined exceptions
// Never write any line below throw or it will give error unreachable statement


import java.util.Scanner;

class Demo {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();

		try {
		
			if(x == 0) 
				throw new ArithmeticException("divide by zero");

			System.out.println(10/x);
		} catch(ArithmeticException ae) {
		
			System.out.println("Exception is thread "+Thread.currentThread().getName());
			ae.printStackTrace();
		}
	}
}
