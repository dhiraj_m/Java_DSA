// 3 errors as exception is already caught in Exception e

class Demo {

	public static void main(String[] args) {
	
		try {
		
			System.out.println(10/0);
		}catch(Exception e) {		//error:- never write parent class									before child exception
		
		}catch(NullPointerException ne) {
		
		} catch(ArithmeticException ae) {
		
		} catch(Exception e) {
		
		}
	}
}
