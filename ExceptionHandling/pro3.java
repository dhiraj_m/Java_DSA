// Default Exception Handler:- When we throw the exception using throws it is handled by the  default exception handler
// If exception is not handled by any ways then DEH gives exception which contains 4 things:-
// 1. Thread Name :- Exception in "main" 
// 2. Exception Name :- java.lang.ArithmeticEception
// 3. Description :- / by zero
// 4. Stack trace :- at Demo.m1() (where the exception is occurred)
// 		     at Demo.main() (returns to method from where that m1() is called)
//
// Why try catch?
// If any method is called which throws exception from 100 methods then we have to use throws on all of that 100 methods hence better way to use try catch in exception method
