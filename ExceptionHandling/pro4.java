// try catch
// NullPointerException

class Demo {

	void m1() {
	
	}
	public static void main(String[] args) {
	
		Demo d = new Demo();
		d.m1();
		d = null;
		try {
		
			d.m1();				// Risky Code
		} catch(NullPointerException obj) {
		
			Demo d2 = new Demo();
			d2.m1();			// Handling code
		}
	}
}
