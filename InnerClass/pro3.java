// **Bytecode
// compiler puts this pointer(of outer class) inside inner class


class Hospital {						// Hospital.class

	class Doctor {						// 'Hospital$Doctor.class'

		Hospital this$0;				//added by compiler

		class MBBS {					// 'Hospital$Doctor$MBBS.class'
		
			Hospitcal$Doctor this$1;		//added by compiler
		}
	}

	class Medical {
	
		Hospital this$0;				//added by compiler
	}
}
