// Whenever there is $ in class name .class file will be single quoted
// Both class parent is object class



class Outer {			// Outer.class
			
	class Inner {		// 'Outer$Inner.class'
	
	}
}

class Demo$ {

}
