// Method local inner class

// If we want inner class when specific method is called then is it used
// Inner class does not exist until we call the method
// Object of such inner class cannot be created outside that method
// this$0 is added by compiler
// Scope of this class is within the method



class Outer {							// Outer.class

	void m1() {
	
		System.out.println("Outer-m1");

		class Inner {					// 'Outer$1Inner.class'
		
			void m1() {
			
				System.out.println("Inner-m1");
			}
		}
		Inner obj = new Inner();
		obj.m1();
	}
	void m2() {
	
		System.out.println("Outer-m2");	
	}
	public static void main(String[] args) {
	
		Outer obj = new Outer();
		obj.m1();
		obj.m2();
	}
}
