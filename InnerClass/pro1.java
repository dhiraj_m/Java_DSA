// 4 types of InnerClasses
// 1.Normal Inner Class
// 2.Method Local Inner Class
// 3.Static Inner Class(Static Nested Class)
// 4.Annonymous Inner Class
// 	-Normal
// 	-Parameterized annonymous inner class(used in Android)

// why inner class?
// whenever there is dependency of one class on other class inner class concept is used
// for e.g. flat : flat is a class which is depend on other class building(inside building class), i.e. we cannot access flat class without accessing building class
// e.g.2 class College and class Department:- Only Department or only class will not be there
// There is class Department inside class College(dependency) and Department will be accessed from inside College
// e.g.3 class Water and class Fish
// e.g.4 class India and class Maharashtra
// e.g.5 ***class Defence and class Navy or class Army



// 1.Normal Inner Class 

class Outer {

	class Inner {
	
		void m1() {
		
			System.out.println("Inner M1");
		}
	}
	void m2() {
	
		System.out.println("Outer m2");
	}
}
class Client {

	public static void main(String[] args) {
	
		//Inner obj2 = new Inner();		Inner class cannot be directly accessed(cannot find symbol)
		
		Outer obj1 = new Outer();
		obj1.m2();

		Outer.Inner obj2 = obj1.new Inner();
		obj2.m1();
	}
}
