// When we write main method in other class then we cannot create object of inner class directly(as we cannot see iner class from outside)
// for that first we have to create object of outer class

// When we write main method inside outer class then we can see inner class directly because we are already inside the outer class
// In this case if inner class is static then we can directly create the object of inner class without creating outer class object and if inner class is non static then we have to create object of outer class to access its inner class


class Outer {

	class Inner {
	
		void m1() {
		
			System.out.println("Inner-m1");
		}
	}
	void m2() {
	
		System.out.println("Outer-m2");
	}
	
	public static void main(String[] args) {
	
		//Inner obj2 = new Inner();		if inner class is static
		
		Inner obj = new Outer().new Inner();
	}
}
