// 3.Static Inner class
// If main method is used outside then we have to reference of outer

class Outer {

	static class Inner {
	
		void m1() {
	
			System.out.println("Inner-m1");
		}
	}
	/*
	public static void main(String[] args) {
	
		Inner obj = new Inner();
		obj.m1();
	}*/
}
class Client {

	public static void main(String[] args) {
	
		Outer.Inner obj = new Outer.Inner();
		obj.m1();
	}
}
