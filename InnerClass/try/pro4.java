class Demo {

	static int x = 10;
	static class Try {
	
		static int x = 20;
		void fun() {
		
			//System.out.println(Demo.this);
		}
		static {
		
			System.out.println("Inner");
		}
	}
	void fun() {
	
	}
	static {
		
		System.out.println("Outer");
	}
	public static void main(String[] args) {
	
		Try c = new Try();
	}
}
