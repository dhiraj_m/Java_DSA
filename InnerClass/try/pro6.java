class Outer {

	static class Inner {
	
		static int x = 10;
	}
}
class Client {

	public static void main(String[] args) {
	
		Outer obj = new Outer();
		System.out.println(Outer.Inner.x);
	}
}
