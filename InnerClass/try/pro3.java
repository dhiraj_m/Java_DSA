class Demo {

}

class Client {

	public static void main(String[] args) {
	
		Demo d1 = new Demo() {				// 'Client$1.class'
		
			Demo d11 = new Demo() {			// 'Client$1$1.class'
			
			
			};
		};
		Demo d2 = new Demo() {				// 'Client$2.class'
		
			Demo d11 = new Demo(){			// 'Client$2$1.class'
			
			};
		};
		Demo d3 = new Demo() {				// 'Client$3.class'
		
		
		};
	}
}
