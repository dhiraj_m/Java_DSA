class Demo {

	void gun() {
			
		System.out.println("gun");
	}
}

class Client {

	public static void main(String[] args) {
	
		Demo obj = new Demo() {
		
			final static int x = 10;
			Demo fun() {
			
				System.out.println("In fun");
				return new Client$1();
			}
			void gun() {
			
				System.out.println("Annonymous gun"+x);
			}
			
		}.fun();
		obj.gun();
	//	System.out.println(obj.x);
	}
}
