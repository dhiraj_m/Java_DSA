class Parent {

	int a = 10;
	static int b = 20;
}
class Child extends Parent {

	int a = 30;
	static int b = 40;
}
class Main {

	public static void main(String[] args) {
	
		Child C = new Child();
		Parent P = new Parent();

		System.out.println(C.a);
		System.out.println(C.b);
		
		System.out.println(P.a);
		System.out.println(P.b);
	}
}
