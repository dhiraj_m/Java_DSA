class Parent {

	static int a = 10;
	int b = 20;

	static {
	
		System.out.println("Parent static");
	}
	Parent() {
	
		System.out.println("Parent constructor");
	}
	void fun() {
	
		System.out.println(a);
		System.out.println(b);
	}
}
class Child extends Parent {

	static int a = 30;
	int b = 40;

	static {
	
		System.out.println("Child static");
	}
	Child() {
	
		System.out.println("Child constructor");
	}
/*	
	void fun() {
	
		System.out.println(a);
		System.out.println(b);
	}*/
}
class Main {

	public static void main(String[] args) {
	
		Child c = new Child();
		c.fun();
	}
}
