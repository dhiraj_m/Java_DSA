class Parent {

	int x = 10;
	static int y = 20;

	void fun() {
	
		System.out.println("In fun");
	}
}
class Child extends Parent {

	int x = 30;
	static int y = 40;
	void access() {
	
		//System.out.println(super);
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
	static void gun() {
	
		System.out.println("In gun of child");
		//super.fun();
	}
}
class Client {

	public static void main(String[] args) {
	
		Child c = new Child();
		c.access();
		c.gun();

		Parent p = new Parent();
		System.out.println(p.x);
	}
}
