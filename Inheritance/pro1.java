class Parent {

	static {
	
		System.out.println("In parent static");
	}
	Parent() {
	
		System.out.println("In Parent Constructor");
	}
}
class Child extends Parent {

	static {
	
		System.out.println("In Child Static");
	}
	Child() {
	
		System.out.println("In Child Contructor");
	}
}
class Main {

	public static void main(String[] args) {

		Parent p = new Parent();
		Child c = new Child();	
	}
}

