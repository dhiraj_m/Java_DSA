class Parent {

	static {
	
		System.out.println("PArent Static");
	}
	Parent() {
	
		System.out.println("Parent Constructor");
	}
	void fun() {
	
		System.out.println("In fun");
	}
}
class Child {

	//Parent p = new Parent();
	static {
	
		System.out.println("Child Static");
	}
	Child() {
	
		Parent p = new Parent();
		System.out.println("Child Constructor");
		//Parent p = new Parent();
	}
}
class Main {

	public static void main(String[] args) {
	
		Child c = new Child();
	//	c.p.fun();
	}
}
