interface Demo1 {

	static void fun() {
	
		System.out.println("In fun Demo1");
	}
	default void gun() {
	
		System.out.println("In gun Demo1");
	}
}
interface Demo2 {

	static void fun() {
	
		System.out.println("In fun Demo2");
	}
	default void gun() {
	
		System.out.println("In gun Demo2");
	}
}
class DemoChild implements Demo1,Demo2 {

}
class Client {

	public static void main(String[] args) {
	

	}
}
