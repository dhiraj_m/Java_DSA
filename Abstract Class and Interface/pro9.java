// default in interface is used 
// 1. if multiple class implement interface and having same method body then we can give body to that method in interface using and all child classes don't need to give the body
// 2. default method can be overrided in child
// 3. default method in parent cannot be called once it is overrided 


interface Demo1 {

	default void fun() {
	
		System.out.println("Fun-Demo1");
	}
}
interface Demo2 {

	default void fun() {
	
		System.out.println("Fun-Demo2");
	}
}
class DemoChild implements Demo1,Demo2 {


}
class Client {

	public static void main(String[] args) {
	
		Demo1 d = new DemoChild();		//Compile time binding with Parent fun()
							//Runtime call to child fun()
		d.fun();
	}
}
