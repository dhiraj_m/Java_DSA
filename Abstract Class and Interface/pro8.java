interface Demo {

	default void fun() {
	
		System.out.println("Fun-Demo");
	}
}
class DemoChild implements Demo {

	public void fun() {
	
		System.out.println("fun-DemoChild");
	}
}
class Client {

	public static void main(String[] args) {
	
		Demo d = new DemoChild();		//Compile time binding with Parent fun()
							//Runtime call to child fun()
		d.fun();
	}
}
