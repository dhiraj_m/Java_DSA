// If we does not give body to the function we have to declare it as abstract
// And if we declare method as abstract then we have to declare class as abstract


abstract class Parent {

	abstract void fun();
}
class Child extends Parent {


}
class Client {

	public static void main(String[] args) {
	

	}
}
