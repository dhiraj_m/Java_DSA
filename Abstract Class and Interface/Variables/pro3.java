interface A {

	int x = 10;
}
interface B {

	int x = 20;
}
class Child implements A,B {

	int x = 30;

	void fun() {
	
		System.out.println(A.x);		//10
		System.out.println(B.x);		//20
		System.out.println(x);			//30
	}
}
