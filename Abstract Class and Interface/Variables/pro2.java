// variable in interface is bipushed as many times as it is accessed



interface Demo {

	int x = 10;			
	void fun();
}
class DemoChild implements Demo {

	int y = x;					//bipush 10 inside contructor
	public void fun() {
	
		System.out.println(x);			//bipush 10
		System.out.println(x);			//bipush 10
		System.out.println(Demo.x);		//bipush 10
	}
}

class Client {

	public static void main(String[] args) {
	
		System.out.println(Demo.x);
		Demo d = new DemoChild();
		d.fun();
	}
}
