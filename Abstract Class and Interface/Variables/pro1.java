// public :- child class may be present in other folders
// static :- because interface cannot be instantiated and hence only using interface we can access that variable
// final :- static variables are class variables hence to make sure other classes will not change its value.
// interface does not have static block
// variables of interface are initialized in a class where it is accessed(in stack frame where it is accessed)


interface Demo {

	int x = 10;			// public static final int x;
	void fun();
}
class DemoChild implements Demo {

	public void fun() {
	
		System.out.println(x);
	}
}

class Client {

	public static void main(String[] args) {
	
		System.out.println(Demo.x);
		Demo d = new DemoChild();
		d.fun();
	}
}



/*
class Demo2 {

	static int x = 10;		//static block{}
					//	bipush 10;
}*/
