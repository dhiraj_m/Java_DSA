// Marker Interface :- To provide special meaning to the class we can implement marker interface in our class
// Marker interfaces does not have any method
// JVM gives special functionality to the class when we implement these interfaces
//
// 1. Serializable
// 2. RandomAccess
// 3. Cloneable
// 4. EventListener
//
//
// for e.g in Array,ArrayList etc we access the data randomly using index no.
// in this case this classes implements RandomAccess interface internally to tell JVM that the data can be accessed randomly
// for e.g String class implements Serializable interface so that JVM can give the sequential memory to String
