// static in interface is used 
// 1. if we don't want to override that method in child 
// 2. if we don't want to change it but if we want to access it then can be accessed using interface name only
// 3. Child does not inherit that method


interface Demo {

	static void fun() {
	
		System.out.println("Fun-Demo");
	}
}
class DemoChild implements Demo {

	void fun() {
	
		System.out.println("fun-DemoChild");
		Demo.fun();
	}
}
class Client {

	public static void main(String[] args) {
	
		DemoChild d = new DemoChild();		
		d.fun();
	}
}
