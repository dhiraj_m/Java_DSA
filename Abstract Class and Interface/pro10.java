// static method in class is inherited by child
// child does not inherit static method from interface


interface Demo {

	static void fun() {
	
		System.out.println("Demo fun");
	}
}
class DemoChild implements Demo {

	
}
class Client {

	public static void main(String[] args) {
	
		DemoChild dc = new DemoChild();
		dc.fun();				//Cannot find symbol 
		
		Demo d = new DemoChild();
		d.fun();				//error 
	}
}



/*
class Demo {

	static void fun() {
	
		System.out.println("Demo fun");
	}
}
class DemoChild extends Demo {

	
}
class Client {

	public static void main(String[] args) {
	
		DemoChild dc = new DemoChild();
		dc.fun();				//no error
	}
}*/
