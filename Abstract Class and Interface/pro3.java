// Interface - To support Multiple inheritance and it is 100% abstraction 
// No constructor - as it is a incomplete class
// Hence cannot be instantiated
// But we can use reference of it

interface Demo {

	void fun();	//public abstract void fun()
	
	static void gun() {		// java version 1.8
	
	
	}
	default void mun() {		// java version 1.8
	

	}
}
class DemoChild implements Demo {

	public void fun() {
	
	
	}
}
