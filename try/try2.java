class Parent {

	static int x = 500;
	int a = 10;
	int b = 20;
	void gun() {
	
		
	}
}
class Child extends Parent {

	int a = 30;
	int b = 40;

	void fun() {

		System.out.println(super.a);
		System.out.println(super.b);
		System.out.println(this);
		super.gun();
	}/*
	void gun() {
	
		System.out.println("Child");
	}*/
	public static void main(String[] args) {
	
		Child c = new Child();
		c.a = 100;
		c.b = 200;

		c.fun();
	}
}
