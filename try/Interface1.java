
interface A {
	
	int x = 10;
	void m1();
}
interface B {

	int x = 20;
	void m1();
}
class Demo implements A,B {
	
	int x = 30;
	public void m1() {
	
		System.out.println("In m1");
	}
	public static void main(String[] args) {
	
		Demo obj = new Demo();
		obj.m1();
		System.out.println(obj.x);
	}
}
