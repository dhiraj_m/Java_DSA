class StringBuf {

	public static void main(String[] args) {
	
		StringBuffer sb = new StringBuffer("Dhiraj");
		System.out.println(sb.capacity());

		System.out.println(System.identityHashCode(sb));

		sb.append("12345678912345678");
		System.out.println(sb.capacity());

		System.out.println(System.identityHashCode(sb));
		System.out.println(sb);

	}
}
